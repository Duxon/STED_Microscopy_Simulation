/* simSTEDparam.h
 * Header file for Class simSTEDparam
 */

#ifndef __simSTEDparam_h__
#define __simSTEDparam_h__

#include <gsl/gsl_matrix.h>

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <iomanip> 
#include <string>
#include <math.h>
#include <omp.h>

#include "simSTEDtools.h"

#define NPARAM 30
const std::string simSTEDparam_description[NPARAM] = {"excitation cross section", \
    "STED cross section", "depletion cross section ", "two photon excitation", \
    "two photon exictation (STED)",\
    "blaching from singlet", "bleaching from singlet (STED)",\
    "blaching from triplet", "bleaching from triplet (STED)", \
    "total decay rate", "fluorescence decay rate", \
    "inter system crossing", "triplet rate", "bleaching rate",\
    "pulse energy excitation", "pulse energy STED",\
    "excitation intensity density", "STED intensity density", \
    "average excitation intensity", "average STED intensity",\
    "wavelength excitation", "wavelength STED", "numerical aperture",\
    "cycle length", "excitation start", "excitation end",\
    "STED start", "STED end", "start of signal collection", "end of signal collection"};
const std::string simSTEDparam_paramName[NPARAM] = { \
    "param_sigma_e", "param_sigma_s", "param_sigma_d", "param_sigma_2e", "param_sigma_2s", \
    "param_sigma_bes", "param_sigma_bss", "param_sigma_bet", "param_sigma_bst",\
    "param_k_tot", "param_k_f", "param_k_isc", "param_k_t", "param_k_b", \
    "power_pulse_e", "power_pulse_s", \
    "power_intensity_e", "power_intensity_s", "power_average_e", "power_average_s",\
    "power_lambda_e", "power_lambda_s", "power_NA", \
    "power_cycle_length", "power_excitation_start", "power_excitation_end", \
    "power_depletion_start", "power_depletion_end", "signal_collection_start", "signal_collection_end" };
        
 const double hPlanck = 6.62607004e-34;
 const double cLight = 299792458;
        
 class simSTEDparam {
 public:
     enum param { SIGMA_E, SIGMA_S, SIGMA_D, SIGMA_2E, SIGMA_2S, \
         SIGMA_BES, SIGMA_BSS, SIGMA_BET, SIGMA_BST, \
         K_TOT, K_F, K_ISC, K_T, K_B, \
         PULSE_E, PULSE_S,  
         I_E, I_S, I_E_AVG, I_S_AVG,\
         LAMBDA_E, LAMBDA_S, NA, \
         T_CYLCE_LEN, T_EXC_START, T_EXC_END, \
         T_STED_START, T_STED_END,\
         T_SIG_START, T_SIG_END, UNKNOWN };
         enum matrix_type { NOLIGHT=0, EXCITATION_ONLY=1, STED_ONLY=2, EXC_STED=3, \
             NOLIGHT_SIGNAL = 4, EXCITATION_SIGNAL = 5, STED_SIGNAL = 6, EXC_STED_SIGNAL = 7, UNDEFINED 
         };	
         static const int has_excitation = 1, has_depletion = 2, collect_signal = 4, is_constant = 0;
         
         gsl_matrix* getTransitionMatrix(enum matrix_type, double, double);
         gsl_matrix* getTransitionMatrix(int, double , double);
         gsl_matrix* getTransitionMatrix(int);
         
         double getParam(param);
         double getParam(const string&);
         int setParam(param, double, bool);
         int setParam(param, double);
         int setParam(const string&, double);
         void generateTimingList(void);
         void updateParameter(int);
         
         int getNtiming();
         matrix_type getType(int);
         bool hasNolaser(int);
         bool hasExcitation(int);
         bool hasDepletion(int);
         
         string printParam(void);
         
         double getAiryExc(void);
         double getAirySTED(void);
         
 private:
     
     struct {
         // absorption cross sections, one photon case
         double sigmaE, sigmaS, sigmaD;
         // absorption cross sections, two photon case
         double sigma2E, sigma2S;
         // active bleaching from singlet or triplet
         double sigmaBES, sigmaBSS, sigmaBET, sigmaBST;
         // decay rates
         double k_tot, k_f, k_ISC, k_T, k_B;
         // airy dimensions
         double airy_exc, airy_STED;
     } rates;
     struct {
         // energy per pulse
         double pE, pS; 
         // average power
         double IE_avg, IS_avg;
         // power density
         double IE, IS;   
         // power per photon
         double photonEE, photonES;
         // power density in photons
         double IEp, ISp;
         // wavelengths
         double lambdaE, lambdaS, NA;
         // timing
         double pulse_length, excitation_start, excitation_end, \
         depletion_start, depletion_end, signal_collection_start, signal_collection_end;
     } power;
     
     int N_timing; // how many matrices are necessary to describe pulse sequence
     matrix_type matrix[10];
     double tau[10];
     gsl_matrix* transition_matrix;
     
     //void addToMatrix(gsl_matrix*, int, int, double);
     void calculateAiryunits();
     void calculatePhotonEnergy();
     void calculatePowerDensity();
     void calculatePowerDensityPerPhoton();
     void calculatePowerPerPulse();	
     void calculateTransitionMatrix();
     
 public: 
     simSTEDparam();
     simSTEDparam(const simSTEDparam&);
     ~simSTEDparam();
 };
 
 #endif
 
 // EOF