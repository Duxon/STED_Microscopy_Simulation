function [J, snr] = generateNoisyImage(Image, lambda)
% Generates an noisy overlay with given size ...
% J = imnoise(I,'poisson') generates Poisson noise from the data 
% instead of adding artificial noise to the data. 
% If I is double precision,
% then input pixel values are interpreted as means of 
% Poisson distributions scaled up by 1e12. For example,
% if an input pixel has the value 5.5e-12, 
% then the corresponding output pixel will be generated 
% from a Poisson distribution with mean of 5.5 
% and then scaled back down by 1e12.

%lambda = 400;  % straylight MLE

Image = double(Image);

factor = 1e-12; % reverse scaling factor ...
                % according to MATLAB documentation
J = imnoise(Image*factor,'poisson')/factor;


% Noise induced by straylight, bleed-through etc.
% ... lambda needs to be extracted from data (dark ROI)
[xWidth, yWidth] = size(Image);
noiseMask = poissrnd(lambda, xWidth, yWidth);
J = J + noiseMask;

snr = calculateSNR(J, Image);

% figure
% imagesc(J), colorbar;


function SNR = calculateSNR(image, reference)
% https://nl.mathworks.com/help/images/ref/psnr.html

normFactor = max(reference(:));
[SNR, ~] = psnr(image/normFactor, reference/normFactor);









%EOF