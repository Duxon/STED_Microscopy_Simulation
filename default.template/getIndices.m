function indices = getIndices(prefix)

global file_prefix    % if other than 'toto', set it in global variable
if isempty(file_prefix), file_prefix='toto'; end
if (nargin>0) && ~isempty(prefix), file_prefix = prefix; end

dd=dir([file_prefix,'_*_result.img']);
file_prefix2 = strsplit(file_prefix,'/');
file_prefix2 = file_prefix2(end);

for ii=1:length(dd)
    indices{ii} = sscanf(dd(ii).name, [file_prefix2{1}, '_%[0-9a-zA-Z]_result.img']);
end

%% EOF    
