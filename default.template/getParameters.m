function param = getParameters(indices, parameters)

global file_prefix    % if other than 'toto', set it in global variable
if isempty(file_prefix), file_prefix='toto'; end


param = struct('name', '', 'value', [], 'log', false);
if nargin<2, return; end
if ischar(indices) indices = { indices }; end
if ischar(parameters) parameters = { parameters }; end
Ni = length(indices);
Np = length(parameters);

% preset outputs to NaN (will be overwritten unless there is an error)
dummy = nan(1,Ni);
for ii=1:Np, param(ii).name=parameters{ii}; param(ii).value=dummy; end

for ii=1:Ni
    filename = [file_prefix,'_',indices{ii},'.param'];
    fid = fopen(filename,'r');
    if (fid > 0)    % problem opening file -> ignore
        finished=false;
        while ~(feof(fid) || finished) 
            str = fgetl(fid);
            if length(str)>0 
                if (str(1)=='=')    % a line of '=' signifies end of parameters
                    finished=true; 
                else
                    val = regexp(str,'([a-zA-Z_]*):\s?([0-9e+-.]*)\s?', 'tokens');
                    % find parameter name % value
                    if ( length(val)>0 && length(val{1})>1 )
                        for jj=1:Np
                            if strcmp(val{1}{1},parameters{jj})
                                param(jj).value(ii)=str2double(val{1}{2});
                            end % if parameter recognized
                        end % or all parameters
                    end % if regexp returned good results
                end % line start with '='
            end % if str contains data
        end % while not eof
        
        fclose(fid);
    end
end

%% EOF