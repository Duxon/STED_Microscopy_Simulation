function [toto, totoB] = displayData(indices, saveFiles)
  
  if nargin<2, saveFiles = false; end
  if ischar(indices) indices = { indices }; end
  lI = length(indices);
  
  global file_prefix    % if other than 'toto', set it in global variable
  if isempty(file_prefix), file_prefix='toto'; end

  od = []; %load(['toto_',indices{1},'_input.img']);
  
%  figure

  % Preallocation
  toto = cell(1,lI);
  totoB = cell(1,lI);

  for ii=1:lI
    
    currfile = [file_prefix,'_',indices{ii}];
    toto{ii} = load([currfile,'_result.img']);
    if ~all(size(toto{ii})==size(od))
        od = load([currfile,'_input.img']);  
    end
    totoB{ii} = load([currfile,'_bleached.img'])./od;
    
    if (saveFiles)
        clf
        subplot(1,2,1), imagesc(toto{ii}); title(indices{ii}); colorbar
        axis square
        subplot(1,2,2); imagesc(totoB{ii}); title(indices{ii}); colorbar
        axis square
        print(gcf, [currfile,'.png'], '-r300');        
    else
      %  subplot(lI,2,2*ii-1); imagesc(toto{ii}); title(indices{ii}); colorbar
      %  subplot(lI,2,2*ii); imagesc(totoB{ii}); title(indices{ii}); colorbar
    end
  end
    
%% EOF
