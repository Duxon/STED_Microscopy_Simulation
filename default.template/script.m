% Lambda was measured to be around 1.5515. That's the background noise
% in terms of a poisson noise (per pixel).
lambda = 1.5515;

indices = getIndices('data/toto');
param = getParameters(indices, 'power_average_e');

display('Parameters aquired. Continueing with loading image files...')
[toto, totoB] = displayData(indices, 0);
 
display('Images loaded. Continueing with calculating [avgSig, avgBleach, fwhm]...')
[avgSig, avgBleach, fwhm, psnr, totoJ] = ...
    analyzeData(toto, totoB, 128, (1:255), 20, param, lambda);


plot(param.value,psnr)
xlabel('P_{Excitation} [W]')
ylabel('PSNR [dB]')
title('PSNRvsP_{excitation}')