function [avgSig, avgBleach, fwhm, psnr, totoJ] = analyzeData(toto, totoB, xind, yind, nm, parameter, lambda)
  
  
  figure
  extraXvals = false;
  if (nargin==6) && (length(parameter.value)==length(toto))
    extraXvals=true;
    subplot(2,2,1); 
  else
    subplot(2,1,1); 
  end
  hold on
  
  if length(xind)>1
    x=nm*(xind-mean(xind));
  else
    x=nm*(yind-mean(yind));
  end
    
  N = length(toto);
  cl=min(1, max(0, interp1(linspace(0,1,5),...
      [1 0 0; 1 1 0; 0 1 0; 0 1 1; 0 0 1], ...
      linspace(0,1,N))));
      
  for ii=1:N
    indSig = find(toto{ii}>0);
    avgSig(ii,:) = [max(toto{ii}(:)), mean(toto{ii}(indSig)), std(toto{ii}(indSig))];
    indBleach = find(totoB{ii}>0);
    avgBleach(ii,:) = [max(totoB{ii}(:)), mean(totoB{ii}(indBleach)), std(totoB{ii}(indBleach))];
    
    curve = toto{ii}(xind, yind);
    plot(x,curve, 'color',cl(ii,:));
    
    % fwhm(ii)=sum(curve>(max(curve)/2))*nm;
    f = fit(yind.',curve.','gauss1');
    fwhm(ii) = 2.3548*f.c1*nm;
    
    % Calculate PSNR
    [totoJ{ii}, psnr(ii)] = generateNoisyImage(toto{ii}, lambda);
    
  end
  
  xlabel('position'); ylabel('signal');
 
  subplot(2,2,3);
  plot(avgSig(:,1),avgBleach(:,2),'*');
  xlabel('signal'); ylabel('bleaching');
  
  subplot(2,2,4);
  plot(avgSig(:,1),fwhm,'*');
  xlabel('signal'); ylabel('fwhm');
  
  if (extraXvals)
    subplot(2,2,2);
    xval = parameter.value; xtext = parameter.name;
    if (isfield(parameter,'log') && parameter.log)
        ind = (xval > 0);
        xval(ind) = log10(xval(ind));
        xval(~ind) = NaN;
        xtext = ['log ',xtext];
    end
    [ax, h1, h2] = plotyy(xval, avgSig(:,1), xval, avgBleach(:,2));
    
    xlabel(ax(1), xtext,'interpreter','none'); ylabel(ax(1), 'signal');
    ylabel(ax(2), 'bleach');
  end
  %% EOF