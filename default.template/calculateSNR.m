function SNR = calculateSNR(image, reference)
% https://nl.mathworks.com/help/images/ref/psnr.html

normFactor = max(reference(:));
[SNR, ~] = psnr(image/normFactor, reference/normFactor);


% % 1st attempt (failed)
% roi_edge =  30; % use even number
% roi_edge = roi_edge - 1;
% Iwidth = length(Image);
% roi_begin = floor(Iwidth/2) -  floor(roi_edge/2);
% roi_end = roi_begin + roi_edge;
% 
% Signal_roi = Image(roi_begin:roi_end, roi_begin:roi_end);
% Signal = sum(Signal_roi(:));
% Background = Image(1:roi_edge+1,1:roi_edge+1);
% 
% Signal = Signal - (roi_edge+1)^2 * mean(Background(:));
% Noise = std(Background(:));
% 
% % fprintf('Noise = %f \n',Noise)