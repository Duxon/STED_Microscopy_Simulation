OBJECTS = simSTEDtools.o simSTEDparam.o simSTEDfluorophore.o simSTED.o

LIBS = -lgsl -lgslcblas -fopenmp
CXX = g++
CXXFLAGS = -O2 -Wall -fopenmp -g

simSTED	: $(OBJECTS)
	$(CXX) $(CXXLAGS) -o simSTED $(OBJECTS) $(LIBS) 
all	: simSTED

.PHONY : clean

clean :
	rm -f *~ *.o
