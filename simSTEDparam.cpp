/*
 * simSTED.cpp - code for fluorophore
 */

#include "simSTEDfluorophore.h"
using namespace std;
// externals
double simSTEDparam::getAiryExc() {
	
	return rates.airy_exc;
}
double simSTEDparam::getAirySTED() {
	return rates.airy_STED;
}

double simSTEDparam::getParam(param parameter) {
	switch(parameter) {
		case SIGMA_E: return rates.sigmaE;
		case SIGMA_S: return rates.sigmaS;
		case SIGMA_D: return rates.sigmaD;
		case SIGMA_2E: return rates.sigma2E;
		case SIGMA_2S: return rates.sigma2S;
		case SIGMA_BES: return rates.sigmaBES;
		case SIGMA_BSS: return rates.sigmaBSS;
		case SIGMA_BET: return rates.sigmaBET;
		case SIGMA_BST: return rates.sigmaBST;
                case K_TOT: return rates.k_tot;
		case K_F: return rates.k_f;
		case K_ISC: return rates.k_ISC;
		case K_T: return rates.k_T;
		case K_B: return rates.k_B;
		case PULSE_E: return power.pE;
		case PULSE_S: return power.pS;
                case I_E_AVG: return power.IE_avg;
                case I_S_AVG: return power.IS_avg;
		case I_E: return power.IE;
		case I_S: return power.IS;
		case LAMBDA_E: return power.lambdaE;
		case LAMBDA_S: return power.lambdaS;
		case NA: return power.NA;
                case T_CYLCE_LEN: return power.pulse_length;
		case T_EXC_START: return power.excitation_start;
		case T_EXC_END: return power.excitation_end;
		case T_STED_START: return power.depletion_start;
		case T_STED_END: return power.depletion_end;
		case T_SIG_START: return power.signal_collection_start;
		case T_SIG_END: return power.signal_collection_end;
		default: return NAN;
	}
	//return NAN();
}
double simSTEDparam::getParam(const string& parameter) {
	for (int ii=0; ii<NPARAM; ii++) {
		if (stringsEqualCI(simSTEDparam_paramName[ii], parameter)) {
			return getParam((param)ii);
		}
	}
	return NAN;
}

int simSTEDparam::setParam(param parameter, double val, bool do_update) {
	int need_update = 0; // see update() for meaning of this parameter
	switch(parameter) {
		case SIGMA_E: rates.sigmaE=val; need_update = 32; break;
		case SIGMA_S:  rates.sigmaS=val; need_update = 32; break;
		case SIGMA_D:  rates.sigmaD=val; need_update = 32; break;
		case SIGMA_2E:  rates.sigma2E=val; need_update = 32; break;
		case SIGMA_2S:  rates.sigma2S=val; need_update = 32; break;
		case SIGMA_BES:  rates.sigmaBES=val; need_update = 32; break;
		case SIGMA_BSS:  rates.sigmaBSS=val; need_update = 32; break;
		case SIGMA_BET:  rates.sigmaBET=val; need_update = 32; break;
		case SIGMA_BST:  rates.sigmaBST=val; need_update = 32; break;
        case K_TOT:  rates.k_tot=val; need_update = 32; break;
		case K_F:  rates.k_f=val; need_update = 32; break;
		case K_ISC:  rates.k_ISC=val; need_update = 32; break;
		case K_T:  rates.k_T=val; need_update = 32; break;
		case K_B:  rates.k_B=val; need_update = 32; break;
		case PULSE_E:  power.pE=val; power.IE_avg=-1.0; need_update = 24; break;
		case PULSE_S:  power.pS=val; power.IS_avg=-1.0; need_update = 24; break;
		case I_E_AVG: power.IE_avg = val; need_update=28; break;
        case I_S_AVG: power.IS_avg = val; need_update=28; break;
		case I_E:  power.IE=val; power.IE_avg=-1.0; power.pE=-1.0; need_update = 28; break;
		case I_S:  power.IS=val; power.IS_avg=-1.0; power.pS=-1.0; need_update = 28; break;
		case LAMBDA_E:  power.lambdaE=val; need_update=19; break;
		case LAMBDA_S:  power.lambdaS=val; need_update=19; break;
		case NA:  power.NA=val; need_update=19; break;
        case T_CYLCE_LEN:  power.pulse_length=val; need_update=64; break;
		case T_EXC_START:  power.excitation_start=val; need_update=64; break;
		case T_EXC_END:  power.excitation_end=val;need_update=64; break;
		case T_STED_START:  power.depletion_start=val;need_update=64; break;
		case T_STED_END:  power.depletion_end=val; need_update=64; break;
		case T_SIG_START:  power.signal_collection_start=val; need_update=64; break;
		case T_SIG_END:  power.signal_collection_end=val; need_update=64; break;
		default: return -1;
	}
	if (do_update) {
		updateParameter(need_update);
	}
	//cout << "setting parameter " << simSTEDparam_paramName[parameter] << " to value " << val << endl;
	return 0;
}
int simSTEDparam::setParam(param parameter, double val) {
	return setParam(parameter, val, false);
}
int simSTEDparam::setParam(const string& parameter, double val) {
	for (int ii=0; ii<NPARAM; ii++) {
		if (stringsEqualCI(simSTEDparam_paramName[ii], parameter)) {
			return setParam((param)ii, val);
		}
	}
	return -1;
}

gsl_matrix* simSTEDparam::getTransitionMatrix(matrix_type mat_type, double attE, double attS) {
	gsl_matrix* M;
	M = gsl_matrix_alloc(5,5);
	gsl_matrix_memcpy(M, transition_matrix);
	
	if (!( (int) mat_type & collect_signal)) {
		gsl_matrix_set(M, 3, 1, 0); // suppress signal
	}
	if ( (int) mat_type & has_excitation) {
		double val, ie;
		ie = power.IEp*attE;
		// excitation singlet ground  <->  singlet excited state
		val = (rates.sigmaE + rates.sigma2E*ie)*ie;
		addToMatrix(M, 0, 0,-val);
		addToMatrix(M, 1, 0, val);
		// intensity dependent bleaching from singlet excited state
		val = rates.sigmaBES * ie;
		addToMatrix(M, 1, 1,-val);
		addToMatrix(M, 4, 1, val);
		// intensity dependent bleaching from triplet state
		val = rates.sigmaBET * ie;
		addToMatrix(M, 2, 2,-val);
		addToMatrix(M, 4, 2, val);
	}
	if ( (int) mat_type & has_depletion) {
		double val, is;
		is = power.ISp*attS;
		// excitation singlet ground  <->  singlet excited state
		val = (rates.sigmaS + rates.sigma2S*is)*is;
		addToMatrix(M, 0, 0,-val);
		addToMatrix(M, 1, 0, val);
		// depletion 
		val = rates.sigmaD * is;
		addToMatrix(M, 0, 1, val);
		addToMatrix(M, 1, 1,-val);
		// intensity dependent bleaching from singlet excited state
		val = rates.sigmaBSS * is;
		addToMatrix(M, 1, 1,-val);
		addToMatrix(M, 4, 1, val);
		// intensity dependent bleaching from triplet state
		val = rates.sigmaBST * is;
		addToMatrix(M, 2, 2,-val);
		addToMatrix(M, 4, 2, val);
	}
	return M;
}
gsl_matrix* simSTEDparam::getTransitionMatrix(int idx, double attE, double attS) {
	gsl_matrix *M = NULL;
	if (idx<N_timing) {
		M=getTransitionMatrix(matrix[idx],attE,attS);
		gsl_matrix_scale(M, tau[idx]);
	}
	return M;
}
gsl_matrix* simSTEDparam::getTransitionMatrix(int idx) {
	return getTransitionMatrix(idx, 1.0, 1.0);
}

void simSTEDparam::calculateAiryunits() {
	rates.airy_exc = power.lambdaE/(2.0*M_PI*power.NA);
	rates.airy_STED = power.lambdaS/(2.0*M_PI*power.NA);
}
void simSTEDparam::calculatePhotonEnergy(){
    power.photonEE = hPlanck*cLight/power.lambdaE;
    power.photonES = hPlanck*cLight/power.lambdaS;
}
void simSTEDparam::calculatePowerDensity(){
    
    double e2 = (rates.airy_exc*rates.airy_exc)*1e4;	// cm^2 !!
    double s2 = (rates.airy_STED*rates.airy_STED)*1e4;
    
    if (power.IE_avg >= 0) { // first try average power
        power.IE = power.IE_avg / e2 * \
            power.pulse_length/(power.excitation_end-power.excitation_start);
    } else if (power.pE >= 0) { // then try pulse energy
        power.IE = power.pE/(power.excitation_end-power.excitation_start)/e2;
        power.IE_avg = power.pE/power.pulse_length;
    } else {
        cout << "Neither average power nor power per pulse given -- cannot calculate IE" << endl;
    }
    
    if (power.IS_avg >= 0) { // first try average power
        power.IS = power.IS_avg / s2 * \
            power.pulse_length/(power.depletion_end-power.depletion_start);
    } else if (power.pS >= 0) { // then try pulse energy
        power.IS = power.pS/(power.depletion_end-power.depletion_start)/e2;
        power.IS_avg = power.pS/power.pulse_length;
    } else {
        cout << "Neither average power nor power per pulse given -- cannot calculate IS" << endl;
    }
}
void simSTEDparam::calculatePowerPerPulse(){
    
    if (power.IE_avg >= 0) { // first try average power
        power.pE = power.IE_avg * power.pulse_length;
    } else if (power.IE >= 0) { // then try power density
        double e2 = (rates.airy_exc*rates.airy_exc)*1e4; // cm^2
        power.pE = power.IE*(power.excitation_end-power.excitation_start)*e2;
    } else {
        cout << "Neither average power nor power density given -- cannot calculate pE" << endl;
    }
    
    if (power.IS_avg >= 0) { // first try average power
        power.pS = power.IS_avg * power.pulse_length;
    } else if (power.IS >= 0) { // then try power density
        double s2 = (rates.airy_STED*rates.airy_STED)*1e4;	// cm^2
        power.pS = power.IS*(power.depletion_end-power.depletion_start)*s2;
    } else {
        cout << "Neither average power nor power density given -- cannot calculate pE" << endl;
    }
}

void simSTEDparam::calculatePowerDensityPerPhoton(){
    power.IEp = power.IE/power.photonEE;
    power.ISp = power.IS/power.photonES;
}

void simSTEDparam::updateParameter(int mask) {
	if (mask<0) { mask = 127; }
	if (mask & 1)
		calculateAiryunits();
	if (mask & 2)
		calculatePhotonEnergy();
	if (mask & 4) 
		calculatePowerPerPulse();
	if (mask & 8) 
		calculatePowerDensity();
	if (mask & 16)
		calculatePowerDensityPerPhoton();
	if (mask & 32)
		calculateTransitionMatrix();
	if (mask & 64)
		generateTimingList();
}

void simSTEDparam::calculateTransitionMatrix(){
	// these are the intensity independent parts of the transition matrix
	gsl_matrix_set_zero(transition_matrix);
	addToMatrix(transition_matrix, 0, 1, rates.k_tot-rates.k_ISC);
	addToMatrix(transition_matrix, 0, 2, rates.k_T);
	addToMatrix(transition_matrix, 1, 1, -rates.k_tot);
	addToMatrix(transition_matrix, 2, 1, rates.k_ISC);
	addToMatrix(transition_matrix, 2, 2, -rates.k_T-rates.k_B );
	addToMatrix(transition_matrix, 3, 1, rates.k_f); // note that the 'signal' state is virtual
	addToMatrix(transition_matrix, 4, 2, rates.k_B);
}

void simSTEDparam::generateTimingList() {
	double time = 0.0;
	N_timing=0;
	while (time<power.pulse_length) {
		int idx = 0;
		double next_time = power.pulse_length;
		if (time >= power.excitation_start) {
			if (time < power.excitation_end) {
				next_time = min(next_time, power.excitation_end);
				idx += has_excitation;
			}
		} else { next_time = min(next_time, power.excitation_start); }
		if (time >= power.depletion_start) {
			if (time < power.depletion_end) {
				next_time = min(next_time, power.depletion_end);
				idx += has_depletion;
			}
		} else { next_time = min(next_time, power.depletion_start); }
		if (time >= power.signal_collection_start) {
			if (time < power.signal_collection_end) {
				next_time = min(next_time, power.signal_collection_end);
				idx += collect_signal;
			}
		} else { next_time = min(next_time, power.signal_collection_start); }
		tau[N_timing]=next_time-time;
		matrix[N_timing++] = (matrix_type)idx;
		time = next_time;
		if (N_timing > 9) {cout << "run out of timing." << endl; return; }
	}
}
int simSTEDparam::getNtiming(void) { return N_timing; }
simSTEDparam::matrix_type simSTEDparam::getType(int N) { return (N<N_timing)?matrix[N]:UNDEFINED; }
bool simSTEDparam::hasNolaser(int N) { return (N<N_timing)?( (matrix[N] & ~collect_signal) == NOLIGHT ):false;}
//bool simSTEDparam::hasNolaser(int N) { return (N<N_timing)?( matrix[N] == NOLIGHT ):false;}
bool simSTEDparam::hasExcitation(int N) { return (N<N_timing)?(matrix[N] & has_excitation):false;}
bool simSTEDparam::hasDepletion(int N) { return (N<N_timing)?(matrix[N] & has_depletion):false;}

string simSTEDparam::printParam() {
	stringstream pstr;
	
	for (int ii=0; ii<NPARAM; ii++) {
		pstr << simSTEDparam_paramName[ii] << ":\t" << getParam((param) ii) << "\t// " << \
			simSTEDparam_description[ii] << endl;
	}
	pstr << "=======" << endl;
	pstr << "airy units:\t" << rates.airy_exc << "  " << rates.airy_STED << endl;
	pstr << "photon energies: " << power.photonEE << "  " << power.photonES << endl;
	pstr << "power density (photon): " << power.IEp << "  " << power.ISp << endl;
	
	return pstr.str();
}

simSTEDparam::simSTEDparam() {
	
	transition_matrix = gsl_matrix_alloc(5,5);
	
    // absorption cross sections, one photon case
    rates.sigmaE = 5.271e-16;    // at excitation wavelength, cm², based on literature ATTO 647N
    rates.sigmaS = 2.81e-20;    // at STED wavelength, cm², estimate from absorption spectrum
    rates.sigmaD = 5.4792e-17;    // depletion, at STED wavelength, cm², observation by Immaad
    // absorption cross sections, two photon case
    rates.sigma2E = 0e-50;      // not expected to occur
    rates.sigma2S = 0e-50;      // cm⁴/s, 1 GM, estimate
    rates.sigmaBES = 0e-21;
	rates.sigmaBET = 0e-21;
	rates.sigmaBSS = 0e-21;
	rates.sigmaBST = 0e-21;
    // decay rates
    rates.k_tot = 1/3.5e-9;      // 1/s, total decay, based on 3.5 ns life time of fluorophore
    rates.k_f = 0.65*rates.k_tot; // 1/s, fluorescence, quantum efficiency 65% from literature
    rates.k_ISC = 0.01*rates.k_tot; // 1/s, inter-system-crossing, estimate (1:100 of total)
    rates.k_T = 1e5;         // based on 10 us lifetime of triplet state.
    rates.k_B = rates.k_T/100;   // 1% chance of bleaching out of triplet state.
    // power levels (pulse)
    power.pE = 0.0;    // J / excitation pulse
    power.pS = 0.0;     // J / STED pulse
    // average power density
    power.IE_avg = 10e-6;    // 1 µW 
    power.IS_avg = 100e-3;    // 100 mW 
    power.IS = 0.0; 
    power.IE = 0.0;
    // power per photon
    power.photonEE = 0.0; // 3.0702e-19;     // J / photon 532 nm
    power.photonES = 0.0; // 2.686e-19;      // J / photon 750 nm
    // wavelengths
    power.lambdaE = 640e-9;  // excitation wavelength
    power.lambdaS = 780e-9;  // emission wavelength
    power.NA = 1.4;  // numerical aperture
    // timing
    power.pulse_length = 12.5e-9;    // 12.5 ns -> 80 MHz
    power.excitation_start = 0e-9;   // immediate
    power.excitation_end = power.excitation_start + 0.2e-9;    // 200 ps
    power.depletion_start = 0.1e-9;      // 100 ps delay
    power.depletion_end = power.depletion_start + 0.2e-9;     // 200 ps length
	power.signal_collection_start = 0e-9; // immediate
	power.signal_collection_end = power.pulse_length; // all the time
	
    updateParameter(-1); // all but power density (which we assume as given)
}
simSTEDparam::simSTEDparam(const simSTEDparam& p) {
	// copy constructor
	transition_matrix = gsl_matrix_alloc(5,5);
	gsl_matrix_memcpy(transition_matrix, p.transition_matrix);
    
    rates = p.rates;
	power = p.power;
	
	N_timing = p.N_timing;
	for (int ii=0; ii<N_timing; ii++) {
		tau[ii] = p.tau[ii];
		matrix[ii] = p.matrix[ii];
	}
}

simSTEDparam::~simSTEDparam() {
	if (transition_matrix != NULL)
		gsl_matrix_free(transition_matrix);
}