/*
 * simSTEDtools.cpp - tools
 */

#include "simSTEDtools.h"
using namespace std;

gsl_matrix* powMatrix(const gsl_matrix* mat, int N) {
    gsl_matrix *res, *base, *temp;
    
    res = gsl_matrix_alloc(5,5);
    gsl_matrix_set_identity(res);
    base = gsl_matrix_alloc(5,5);
    gsl_matrix_memcpy(base, mat);
    temp = gsl_matrix_alloc(5,5);
    // the algorithm uses the binary representation of N
    // and multiplies if this power of 2 is present
    while (N) {
        if (N&1) { 
            gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
                           1.0, res, base,
                           0.0, temp);
            gsl_matrix_memcpy(res, temp);	
        }
        N >>= 1; 
        if (N) {
            gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
                           1.0, base, base,
                           0.0, temp);
            gsl_matrix_memcpy(base, temp);	
        }
    }
    
    gsl_matrix_free(base);
    gsl_matrix_free(temp);
    return res;
}
void powMatrix_inplace(gsl_matrix* mat, int N) {
    gsl_matrix* temp;
    temp = powMatrix(mat, N);
    gsl_matrix_memcpy(mat, temp);
    gsl_matrix_free(temp);
}

bool stringsEqualCI(const string& a, const string& b) {
    unsigned int N = a.length();
    if (N != b.length()) { return false; }
    for (unsigned int ii=0; ii<N; ii++) {
        if (tolower(a[ii])!=tolower(b[ii])) { return false; }
    }
    return true;
}

unsigned int gcd(unsigned a, unsigned b) {
    return (a < b) ? gcd(b, a) : ((a % b == 0) ? b : gcd(b, a % b));
}
bool areCoprime(unsigned int a, unsigned int b) {
    return (gcd(a,b)==1);
}

double struve(int k, double r) {
    // based on Newman (Mathematics of Computation vol 43(168) pp 551-556 (1984)
    double s;
    switch (k) {
        case 0:
            if (r<=3) {
                double coeff[] = {1.909859164, -1.909855001, 0.687514637, -0.126164557, 0.013828813, -0.000876918};
                double r2, rr; 
                rr = r/3.0;
                r2 = rr*rr;
                s = coeff[0] * rr;
                for (int ii=1; ii<6; ii++) {
                    rr *= r2;
                    s += coeff[ii]*rr;
                }
            } else {
                double coeffA[] = {0.99999906, 4.77228920, 3.85542044, 0.32303607};
                double coeffB[] = {1.0, 4.88331068, 4.28957333, 0.52120508};
                double sA, sB, r2, rr;
                sA=coeffA[0];
                sB=coeffB[0];
                r2=(3.0/r); r2 *= r2;
                rr=1.;
                for (int ii=1; ii<4; ii++) {
                    rr *= r2;
                    sA += coeffA[ii]*rr;
                    sB += coeffB[ii]*rr;
                }
                // note: I had a sign(r) in this formula ?! assume r>0!
                s = gsl_sf_bessel_Y0(r) + 2.0/M_PI/r*sA/sB;
            }
            break;
        case 1: 
            if (r<=3) {
                double coeff[] = {1.909859286, -1.145914713, 0.294656958, -0.042070508, 0.003785727, -0.000207183};
                double r2, rr; 
                rr = r*r/9.0;
                r2 = rr;
                s = coeff[0] * rr;
                for (int ii=1; ii<6; ii++) {
                    rr *= r2;
                    s += coeff[ii]*rr;
                }
            } else {
                double coeffA[] = {1.00000004, 3.92205313, 2.64893033, 0.27450895};
                double coeffB[] = {1.0, 3.81095112, 2.26216956, 0.10885141};
                double sA, sB, r2, rr;
                sA=coeffA[0];
                sB=coeffB[0];
                r2=(3.0/r); r2 *= r2;
                rr=1.;
                for (int ii=1; ii<4; ii++) {
                    rr *= r2;
                    sA += coeffA[ii]*rr;
                    sB += coeffB[ii]*rr;
                }
                // note: I had a sign(r) in this formula ?! assume r>0!
                s = gsl_sf_bessel_Y1(r) + 2.0/M_PI*sA/sB;
            }
            break;
        default: s=NAN; break;
    }
    return s;
}

double airy(double r) {
    double a;
    if (fabs(r)<__DBL_EPSILON__) { 
        a = 1.0/(4*M_PI); 
    } else {
        a = gsl_sf_bessel_J1(r)/r;
        a *= a;
        a /= M_PI;
    }
    return a;
}

double donut(double r) {
    double d;
    if (fabs(r)<__DBL_EPSILON__) {
        d = 0.0;
    } else {
        d = (gsl_sf_bessel_J1(r)* struve(0, r) - gsl_sf_bessel_J0(r)*struve(1,r))/r;
        d *= d;
        d *= 8.0/(M_PI*M_PI);
    }
    return d;
}

string make_timestamp() {
    string out("");

    // Switching from s to ms for very fast simulations (e.g. single fluorophores)
    unsigned  long int millisec;
    struct timespec spec;
    clock_gettime(CLOCK_REALTIME, &spec);
    millisec = 1.0e3 * (long int) spec.tv_sec + round(spec.tv_nsec / 1.0e6); // Convert nanoseconds to milliseconds
    
    while (millisec > 0) {
        unsigned long int newSec = millisec / 36;
        unsigned int remain = (unsigned int) millisec-36*newSec;
        // std::cout << "remainder " << remain << std::endl;
        if (remain<10)
            out.insert(0, 1, (char) '0'+remain);
        // Change to avoid problems with case insensitive filesystems
        //else if (remain < 36)  
        //    out.insert(0, 1, (char) 'A'+remain-10);
        //else
        //    out.insert(0, 1, (char) 'a'+remain-36);
        else
            out.insert(0, 1, (char) 'a'+remain-10);
        millisec = newSec;
    }
    return out;
}

void display_progress(double progress, int N, const string &str) {
    cout << '\r' << str << " [";
    int Nsteps = progress*N;
    for (int ii=0; ii<Nsteps; ii++) cout << '='; 
    for (int ii=Nsteps; ii<N; ii++) cout << '-'; 
    cout << "] " << setprecision(4) << (100*progress) << "%          ";
    cout.flush();
}
void display_progress(double progress, int N) {
    display_progress(progress, N, "");
}
void PrintMatrix(const gsl_matrix *mat) {
    
    for (int ii=0; ii<5; ii++) {
        cout << "( " << gsl_matrix_get(mat, ii, 0);
        for (int jj=1; jj<5; jj++) {
            cout << '\t' << gsl_matrix_get(mat, ii, jj);
        }
        cout << " )" << endl;
    }
    cout << endl;
}

void PrintVector(const gsl_vector *vec) {
    cout << "[ " << gsl_vector_get(vec, 0);
    for (int ii=1; ii<5; ii++)
        cout << '\t' << gsl_vector_get(vec, ii);
    cout << " ]" << endl;
}


// EOF