/* simSTEDfluorophore.h
Header file for Class simSTEDfluorophore
*/

#ifndef __simSTEDfluorophore_h__
#define __simSTEDfluorophore_h__

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>

#include <cstdlib>
#include <stdio.h> 
#include <string>
#include <math.h>

#include "simSTEDparam.h"
#include "simSTEDtools.h"

using namespace std;
        
class simSTEDfluorophore {
public:
    void setParameters(const simSTEDparam&);
    void setState(const gsl_vector*);
    void setS0(double);
    void setS1(double);
    void setT(double);
    void setSignal(double);
    void setB(double);
    
    void getState(gsl_vector *);
    double getS0();
    double getS1();
    double getT();
    double getSignal();
    double getB();
    
    void cycle(double, double, int);
    void cycle(double, double);

private:

    simSTEDparam *parameters;
    int N_timing;
    gsl_vector *state;
    gsl_matrix *tmat[10], *emat[10];
    bool calc_mat[10];
    
    void calculateTransition(int, double, double);

public:
    simSTEDfluorophore();
    simSTEDfluorophore(const simSTEDparam&, const gsl_vector*);
    ~simSTEDfluorophore();
};

#endif

// EOF