/* simSTEDtools.h
   Header file for Class simSTEDparam
*/

#ifndef __simSTEDtools_h__
#define __simSTEDtools_h__

#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>

#include <cstdlib> 
#include <string>
#include <iostream>
#include <iomanip>
#include <math.h>

using namespace std;


inline void addToMatrix(gsl_matrix* mat, int ii, int jj, double val) {
	gsl_matrix_set(mat, ii, jj, gsl_matrix_get(mat, ii, jj)+val);
}
inline void multMatrix(gsl_matrix* mat, double val) {
	// it may be faster to multiply only non-zero elements,
	// but for the time being we leave it like this
	gsl_matrix_scale(mat, val);
}
inline void swapMatrix(gsl_matrix* m1, gsl_matrix* m2) {
	//gsl_matrix* temp;
	//temp=m1; m1=m2; m2=temp;
	gsl_matrix_swap(m1, m2);
}

//#define DEBUG 1

gsl_matrix* powMatrix(const gsl_matrix*, int);
void powMatrix_inplace(gsl_matrix*, int);
bool stringsEqualCI(const string&, const string&);

unsigned int gcd(unsigned int, unsigned int);
bool areCoprime(unsigned int, unsigned int);

double struve(int, double);
double airy(double);
double donut(double);

string make_timestamp();

void display_progress(double, int);
void display_progress(double, int, const string&);
void PrintMatrix(const gsl_matrix*);
void PrintVector(const gsl_vector*);
#endif

// EOF