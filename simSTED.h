
/* simSTED.h
   Header file for the Spider
*/

#ifndef __simSTED_h__
#define __simSTED_h__

#include <iostream>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_matrix.h>

#include <ctime>
#include <fstream>
#include <sstream>
#include <cstring>
#include <cstdlib>
#include <iomanip>

#include <omp.h>

#include <sys/stat.h>

#include "simSTEDparam.h"
#include "simSTEDfluorophore.h"
#include "simSTEDtools.h"

// Command Line Arguments 
enum CLA { NPIXEL, PINHOLE, PXLSIZE, LINESKIP, PARTIALN, PARTIALUPPERTHRESHOLD, PARTIALLOWERTHRESHOLD, PARAMETER, NCYCLE, NREPEAT, \
	INPUTFILE, PARAMFILE, OUTPUTFILEMASK, OUTPUTDIR, HELP, ESTIMATE, QUIET, THREADS, UNKNOWN };
struct fluoInfo {
	int xx, yy;
	double intensity;
};

int nPixel = 128;
double pixelSize = 20e-9;
double pxlPerAiry, pxlPerDonut;
double pinholeD = 1.0; // in Airy units

string fluorophore_inputfile("*none*");
fluoInfo *baseImage;
int Nfluorophore = 256;
int NcyclesperPixel = 80;
int lineSkip = 1;
int Nrepeat = 1;
int partialN = 1;
double partiallowerThreshold = 1;
double partialupperThreshold = 1e10;


simSTEDparam parameter;
string parameter_inputfile("default.param");
gsl_matrix *fluoImage;

string output_mask("toto_");
string output_dir("");

bool estimate_only = false;
bool quiet = false;
int nThreads = 0;

simSTEDfluorophore *fluorophore;


CLA EvaluateToken(char *arg);
int EvaluateArguments(int argc, char **argv);
int ReadParameterFile(string&);
int ReadFluorophoreFile(string&);
void PrintParamterFile(string&);

int printMatrix();

double pinholeAttenuation(double);

# endif

// EOF
