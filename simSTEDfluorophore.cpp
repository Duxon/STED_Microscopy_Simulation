/*
 * simSTEDfluorophore.cpp - code for fluorophore class
 */

#include "simSTEDfluorophore.h"


void simSTEDfluorophore::setParameters(const simSTEDparam& p) {
	if (parameters != NULL) { delete parameters; }
	parameters = new simSTEDparam(p);
	N_timing = parameters->getNtiming();
	for (int ii=0; ii<5; ii++) {
		calc_mat[ii]=!parameters->hasNolaser(ii);
		if (!calc_mat[ii]) { // we only have to do this once}
			calculateTransition(ii,0.0,0.0);
		}
	}
	
}

void simSTEDfluorophore::setState(const gsl_vector *val) {
	gsl_vector_memcpy(state, val);
}
void simSTEDfluorophore::setS0(double val) {
	if (val>=0.0) { gsl_vector_set(state, 0, val); }
}
void simSTEDfluorophore::setS1(double val) {
	if (val>=0.0) {gsl_vector_set(state, 1, val); }
}
void simSTEDfluorophore::setT(double val) {
	if (val>=0.0) { gsl_vector_set(state, 2, val); }
}
void simSTEDfluorophore::setSignal(double val) {
	if (val>=0.0) { gsl_vector_set(state, 3, val); }
}
void simSTEDfluorophore::setB(double val) {
	if (val>=0.0) { gsl_vector_set(state, 4, val); }
}


void simSTEDfluorophore::getState(gsl_vector *vec) { gsl_vector_memcpy(vec, state); }
double simSTEDfluorophore::getS0() 	{ return gsl_vector_get(state, 0); }
double simSTEDfluorophore::getS1() 	{ return gsl_vector_get(state, 1); }
double simSTEDfluorophore::getT() 	{ return gsl_vector_get(state, 2); }
double simSTEDfluorophore::getSignal() 	{ return gsl_vector_get(state, 3); }
double simSTEDfluorophore::getB() 	{ return gsl_vector_get(state, 4); }


void simSTEDfluorophore::cycle(double attE, double attS, int N) {
	
	// prepare matrices
	gsl_matrix *trans, *temp;
	gsl_vector *vec;

	trans = gsl_matrix_alloc(5,5);
	temp = gsl_matrix_alloc(5,5);
	vec = gsl_vector_alloc(5);
	
	// unless already done, calculate transtion matrices & exponentials
	for (int ii=0; ii<N_timing; ii++ ) {
		if (calc_mat[ii]) { // we need to calculate tmat *and* emat
			calculateTransition(ii, attE, attS);
		}
	}
	
	// calculate total transition matrix (product) for one cycle
	//gsl_matrix_set_identity(trans);
	gsl_matrix_memcpy(trans, emat[0]);
	for (int ii=1; ii<N_timing; ii++) {
//		PrintMatrix(tmat[ii]);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
			1.0, emat[ii], trans,
			0.0, temp);		
//		PrintMatrix(emat[ii]);
		//swapMatrix(trans, temp); // does not work?
		gsl_matrix_memcpy(trans, temp);
	}

	#ifdef DEBUG
	if (DEBUG>1) PrintMatrix(trans);
	#endif
	
	// calculate transition matrix for N cycles
	powMatrix_inplace(trans, N);
	#ifdef DEBUG
	if (DEBUG>0) PrintMatrix(trans);
	#endif

	// calculate state after N cycles
	gsl_blas_dgemv(CblasNoTrans, 
		1.0, trans, state,
		0.0, vec);
	
	gsl_vector_memcpy(state, vec);
	#ifdef DEBUG
	if (DEBUG>0) PrintVector(state);
	#endif
	
	gsl_vector_free(vec);
	gsl_matrix_free(temp);
	gsl_matrix_free(trans);
}
void simSTEDfluorophore::cycle(double attE, double attS) { cycle(attE, attS, 1); }


void simSTEDfluorophore::calculateTransition(int N, double attE, double attS) {

	if (tmat[N] != NULL) { gsl_matrix_free(tmat[N]); }
	
	tmat[N] = parameters->getTransitionMatrix(N, attE, attS);
	gsl_linalg_exponential_ss(tmat[N], emat[N], 0);
}


simSTEDfluorophore::simSTEDfluorophore(const simSTEDparam &p, const gsl_vector *s) {

	parameters= new simSTEDparam(p);
	state = gsl_vector_alloc(5);
	gsl_vector_memcpy(state, s);

	for (int ii=0; ii<5; ii++) {

		tmat[ii]=NULL;
		emat[ii]=gsl_matrix_alloc(5,5);
		calc_mat[ii]=!parameters->hasNolaser(ii);
		if (!calc_mat[ii]) { // we only have to do this once}
			calculateTransition(ii,0.0,0.0);
	    }
	}
	N_timing = parameters->getNtiming();
}

simSTEDfluorophore::simSTEDfluorophore() {
		parameters=NULL;
		state=gsl_vector_calloc(5);
		for (int ii=0; ii<10; ii++) {
			tmat[ii]=NULL;
			emat[ii]=gsl_matrix_alloc(5,5);
			calc_mat[ii]=true;
		}
}

simSTEDfluorophore::~simSTEDfluorophore() {
		
		if (parameters!=NULL) { delete parameters; };
		gsl_vector_free(state);
		for (int ii=0; ii<10; ii++) {
			if (tmat[ii]!=NULL) { gsl_matrix_free(tmat[ii]); }
			if (emat[ii]!=NULL) { gsl_matrix_free(emat[ii]); }
		}
}