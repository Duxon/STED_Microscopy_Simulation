/* simSTED main Program
* Version 2.0
*/


#include "simSTED.h"

using namespace std;

void printHelp() {
    cout << "simSTED - simulation of image formation by STED." << endl;
    cout << "possible arguments are (case sensitive!):" << endl;
    cout << "\t-N, --npixel\tnumber of pixels in line, INTEGER, image is assumed square" << endl;
    cout << "\t-I, --inputfile\tfile with NxN table of inital values for fluorophores, STRING" << endl;
    cout << "\t-P, --paramfile\tfile containing standard parameters, STRING;" << endl << "\t\t\tdefault: default.param" << endl;
    cout << "\t-X, --pxlsize\tsize of pixel (in metres, values larger than one" << endl << "\t\t\tare assumed in nm), DOUBLE" << endl;
    cout << "\t-C, --cycles\tnumber of pulse cycles accumulation of a pixel, INTEGER" << endl;
    cout << "\t-R, --repeat\tnumber of repeats (sum-up of signal), INTEGER" << endl;
    cout << "\t-D, --pinhole\tdiameter of detection pinhole in Airy units, DOUBLE" << endl;
    cout << "\t-L, --lineskip\tline skip - will be forced to be coprime with npixel, INTEGER" << endl;
        cout << "\t-pN, --partialN\tdivide total dwell time in partialN subsections and conditionally switch off lasers (see uTh and lTh), INTEGER." << endl;
        cout << "\t-puTh, --partialupperThreshold\tswtich off both lasers after sub-dwell time when signal has reached partialupperThreshold, DOUBLE." << endl;
        cout << "\t-plTh, --partiallowerThreshold\tswtich off both lasers after sub-dwell time when signal is less than partiallowerThreshold, DOUBLE." << endl;
    cout << "\t-O, --outputfile\tprefix for output files, STRING; default: toto_" << endl;
    cout << "\t-D, --outputdir\toutput directory, STRING, default: sim_TIMESTAMP" << endl;
    cout << "\t-p, --param\tindividual parameters for simulation" << endl << "\t\t\tsee simSTEDparam.h for possible values, STRING" << endl;
    cout << "\t-e, --estimate\testimate of simulation complexity, no simulation will be performed." << endl;
    cout << "\t-t, --threads\toverride default number of threads (# fluorophores / 200)." << endl;
    cout << "\t-q, --quiet\tsuppress progress output." << endl;
    cout << "\t-h, --help\tdisplay this screen" << endl;
    
}
CLA EvaluateToken(char *arg) {
    
    if ((strncmp(arg, "-N", 2)==0) | (strncmp(arg, "--npixel", 8)==0))
        return NPIXEL;
    if ((strncmp(arg, "-I", 2)==0) | (strncmp(arg, "--inputfile", 11)==0))
        return INPUTFILE;
    if ((strncmp(arg, "-P", 2)==0) | (strncmp(arg, "--paramfile", 11)==0))
        return PARAMFILE;
    if ((strncmp(arg, "-X", 2)==0) | (strncmp(arg, "--pxlsize", 9)==0))
        return PXLSIZE;
    if ((strncmp(arg, "-C", 2)==0) | (strncmp(arg, "--cycles", 8)==0))
        return NCYCLE;
    if ((strncmp(arg, "-R", 2)==0) | (strncmp(arg, "--repeat", 8)==0))
        return NREPEAT;
    if ((strncmp(arg, "-D", 2)==0) | (strncmp(arg, "--pinhole", 8)==0))
        return PINHOLE;
    if ((strncmp(arg, "-L", 2)==0) | (strncmp(arg, "--lineskip", 10)==0))
        return LINESKIP;
    if ((strncmp(arg, "-pN", 3)==0) | (strncmp(arg, "--partialN", 10)==0))
        return PARTIALN;
    if ((strncmp(arg, "-puTh", 5)==0) | (strncmp(arg, "--partialupperThreshold", 23)==0))
        return PARTIALUPPERTHRESHOLD;
    if ((strncmp(arg, "-plTh", 5)==0) | (strncmp(arg, "--partialupperThreshold", 23)==0))
        return PARTIALLOWERTHRESHOLD;
    if ((strncmp(arg, "-O", 2)==0) | (strncmp(arg, "--outputfile", 12)==0))
        return OUTPUTFILEMASK;
    if ((strncmp(arg, "-D", 2)==0) | (strncmp(arg, "--outputdir", 12)==0))
        return OUTPUTDIR;
    if ((strncmp(arg, "-p", 2)==0) | (strncmp(arg, "--param", 7)==0))
        return PARAMETER;
    if ((strncmp(arg, "-e", 2)==0) | (strncmp(arg, "--estimate", 10)==0))
        return ESTIMATE;
    if ((strncmp(arg, "-q", 2)==0) | (strncmp(arg, "--quiet", 7)==0))
        return QUIET;
    if ((strncmp(arg, "-t", 2)==0) | (strncmp(arg, "--threads", 9)==0))
        return THREADS;
    if ((strncmp(arg, "-h", 2)==0) | (strncmp(arg, "--help", 6)==0))
        return HELP;
    
    return UNKNOWN;
}

int EvaluateArguments(int argc, char **argv){
    
    int N=1;
    
    while (N<argc) {
        switch (EvaluateToken(argv[N++])) {
            case NPIXEL:
                if (N<argc)
                    nPixel=atoi(argv[N++]);
                break;
            case NCYCLE:
                if (N<argc)
                    NcyclesperPixel=atoi(argv[N++]);
                break;
            case NREPEAT:
                if (N<argc)
                    Nrepeat=atoi(argv[N++]);
                break;
            case PXLSIZE:
                if (N<argc)
                    pixelSize = (double) atof(argv[N++]);
                if (pixelSize > 1)          // must be in nanometers
                    pixelSize *= 1e-9;
                break;
            case PINHOLE:
                if (N<argc)
                    pinholeD = (double) atof(argv[N++]);
                break;
            case LINESKIP:
                if (N<argc)
                    lineSkip = atoi(argv[N++]);
                break;
                        case PARTIALN:
                if (N<argc)
                    partialN = atoi(argv[N++]);
                break;
                        case PARTIALUPPERTHRESHOLD:
                if (N<argc)
                    partialupperThreshold = (double) atof(argv[N++]);
                break;
                        case PARTIALLOWERTHRESHOLD:
                if (N<argc)
                    partiallowerThreshold = (double) atof(argv[N++]);
                break;
            case INPUTFILE:
                if (N<argc)
                    fluorophore_inputfile = argv[N++];
                break;
            case PARAMFILE:
                if (N<argc) {
                    parameter_inputfile = argv[N++];
                    ReadParameterFile(parameter_inputfile);
                }
                break;
            case OUTPUTFILEMASK:
                if (N<argc)
                    output_mask = argv[N++];
                break;	
            case OUTPUTDIR:
                if (N<argc) {
                    output_dir = argv[N++];
                    char final_char = output_dir[output_dir.size()-1];
                    if (! ((final_char == '/') ||  (final_char == '\\')) ) {
                        output_dir += '/';  //  make sure final slash is added
                    }
                }
                break;
            case PARAMETER:
                if ((N+1)<argc) {           // parameter has two arguments!
                    if (parameter.setParam(argv[N], atof(argv[N+1]))) {
                        cout << "unknown parameter " << argv[N] << endl;
                    }
                    N+=2;
                }
                break;
            case ESTIMATE:
                estimate_only = true;
                break;
            case QUIET:
                quiet = true;
                break;
            case THREADS:
                if (N<argc)
                    nThreads=atoi(argv[N++]);
                break;

            case HELP:
                printHelp();
                return -1;
            default:
                printHelp();
                cout << " unknown argument " << argv[N-1] << endl;
                return -1;
        }
    }
    return 0;
}

int ReadParameterFile(string &filename) {
    
    ifstream file;
    file.open(filename.c_str());
    
    if (!file.is_open()) {
        cerr << "ERROR opening parameter file " << filename << endl;
        return -1;
    }
    
    while (!file.eof()) {
        char str[128];
        char *param;
        double value;
        file.getline(str, 128);
        if (str[0] == '=') { break; }               // lines starting with '=' mean other information is following
        param = strtok(str, " :\t");
        value = atof(strtok(NULL, " :\t"));
        /* if (param != NULL) { 
        *        cout << param << ": " << value << "( " <<\
        *        parameter.setParam(string(param), value) << " )" << endl; 
    } */
        parameter.setParam(string(param), value);
    }
    
    parameter.updateParameter(-1);
    file.close();
    return 0;
}
int ReadFluorophoreFile(string &filename) {
    ifstream file;
    file.open(filename.c_str());
    
    char str[128] = "# empty";
    int Nread = 0;
    Nfluorophore = 0;

    if (!file.is_open()) {
        cerr << "ERROR opening fluorophore file " << filename << endl;
        return -1;
    }

    while (!file.eof() && str[0]=='#') {
        file.getline(str, 128);                         // get rid of comment lines
    }
    
    if (! file.eof()) {
        Nfluorophore = atoi(str);
        
        if (Nfluorophore <= 0) { return -1; }
        baseImage = new fluoInfo[Nfluorophore];
        
        while (!file.eof() && Nread<Nfluorophore) {
            int xx, yy;
            double intensity;
            
            file.getline(str, 128);
            
            // lines starting with '=' mean other information is following, we are done
            if (str[0] == '=') { break; } 
            
            xx = (int) atoi(strtok(str, " :\t"));
            yy = (int) atoi(strtok(NULL, " :\t"));
            intensity = atof(strtok(NULL, " :\t"));
            
            if (Nread < Nfluorophore) {
                baseImage[Nread].xx = xx;
                baseImage[Nread].yy = yy;
                baseImage[Nread].intensity = intensity;
                Nread ++;
            }
            /*cout << " ** " << Nread << " of " << Nfluorophore << \
            *            "( " << xx << " " << yy << " " << intensity << " )" << endl;*/
        }
    }
    
    file.close();
    return 0;
}

void PrintParameterFile(string &filename) {
    ofstream file;
    file.open(filename.c_str(), std::ofstream::trunc);
    file << parameter.printParam();
    file << "=======" << endl;
    file << "npixel:\t" << nPixel << endl;
    file << "number of cycles per pixel:\t" << NcyclesperPixel << endl;
    file << "repeats:\t" << Nrepeat << endl;
    file << "pixel size:\t" << pixelSize << endl;
    file << "pixel per Airy:\t" << pxlPerAiry << endl;
    file << "pixel per Donut:\t" << pxlPerDonut << endl;
    file << "pinhole diameter:\t" << pinholeD << " (AU)" << endl;;
    file << "line skip:\t" << lineSkip << endl;
        file << "partial N:\t" << partialN << endl;
        file << "partial lower Threshold:\t" << partiallowerThreshold << endl;
        file << "partial upper Threshold:\t" << partialupperThreshold << endl;
    file << "inputfile:\t" << fluorophore_inputfile << endl;
    file << "paramfile:\t" << parameter_inputfile << endl;
    file << "number of fluorophores:\t" << Nfluorophore << endl;
    
    file.close();
}

void PrintImageFile(gsl_matrix *img, string &filename) {
    
    ofstream file;
    file.open(filename.c_str(), std::ofstream::trunc);
    
    for (int ii=0; ii<nPixel; ii++) {
        file << gsl_matrix_get(img, ii, 0);
        for (int jj=1; jj<nPixel; jj++) {
            file << '\t' << gsl_matrix_get(img, ii, jj);
        }
        file << endl;
    }
    
    file.close();
}

void PrintFluorophores(string &filename) {
    ofstream file;
    file.open(filename.c_str(), std::ofstream::trunc);
    
    for (int kk=0; kk<Nfluorophore; kk++) {
        file << baseImage[kk].xx << '\t' << baseImage[kk].yy << '\t' << baseImage[kk].intensity;
        file << '\t' << fluorophore[kk].getS0();
        file << '\t' << fluorophore[kk].getS1();
        file << '\t' << fluorophore[kk].getT();
        file << '\t' << fluorophore[kk].getSignal();
        file << '\t' << fluorophore[kk].getB();
        file << endl;
    }
    
    file.close();
    
}

void PrintEstimate() {
    
    int nTotalPixels = nPixel*nPixel;
    
    cout << "Estimation of simulation complexity:" << endl;
    cout << "\tnumber of pixels:\t" << nTotalPixels << endl;
    cout << "\tnumber of fluorophores:\t" << Nfluorophore << endl;
    cout << "\tnumber of repeats:\t" << Nrepeat << endl;
    
    int nTimings = parameter.getNtiming();
    int constMats = 0;
    for (int ii=0; ii<nTimings; ii++) {
        if (parameter.hasNolaser(ii)) { constMats++; }
    }
    
    cout << "\tnumber of timings:\t" << nTimings << " (of which constant: " << constMats << ")" << endl;
    cout << "\tnumber of simulations:\t" << nTotalPixels*Nfluorophore*Nrepeat << endl;
}

double pinholeAttenuation(double r) {
    
    double fwhmAiry = 3.2327*pxlPerAiry;	               // technically we would need the emission airy dimension
    double pR = pinholeD/2;
    double centerOne = pow(pR,2)/fwhmAiry;
    if (r>fwhmAiry) {
        double wf = exp(-pow(pR-fwhmAiry,2)/2/pow(fwhmAiry,2));
        centerOne *= wf;
        centerOne += (1-wf)*(pR-fwhmAiry);
    } 
    if (r<centerOne) {
        return 1.0;
    } else {
        return 4.0*M_PI*airy((r-centerOne)/fwhmAiry);
    }
    
}

inline void PrintPower(double r) {
    double rA = r/parameter.getAiryExc();
    double rD = r/parameter.getAirySTED();
    cout << r << " nm: " << airy(rA) << " (" << rA << ")\t" << \
    donut(rD) << " (" << rD << ")" << endl;
}
inline bool inImage(fluoInfo &f) {
    return ((f.xx)>=0 && (f.yy>=0) && (f.xx<nPixel) && (f.yy<nPixel));
}
int findLineSkip(int L, int N) {
    if (L == 0) L = (int) ceil(2*pxlPerDonut);
    while (!areCoprime(L, N) && L<N) L++;
    if (L>=N) L=1;
    return L;
}

int main(int argc, char **argv) {
    
    #ifdef DEBUG
    if (DEBUG > 1) {
        ofstream file1;
        file1.open("toto.dat", std::ofstream::trunc);
        for (double x=0.0; x<20; x+=0.01) {
            cout << x << ": " << airy(x) << " " << donut(x) << endl;
            file1 << x << '\t' << airy(x) << '\t' << donut(x) << '\t' << struve(0, x) << '\t'<< struve(1, x) << endl;
        }
        file1.close();
        return -1; 
    }
    #endif
    ofstream file;
    stringstream fns;
    string filename("");
    
    // evaluate parameters
    
    if (EvaluateArguments(argc, argv)!= 0) { return -1;	}
    parameter.updateParameter(-1);
    
    pxlPerAiry = parameter.getAiryExc()/pixelSize;
    pxlPerDonut = parameter.getAirySTED()/pixelSize;
    
    lineSkip = findLineSkip(lineSkip, nPixel);

    // generate list of fluorphores
    if (ReadFluorophoreFile(fluorophore_inputfile)<0) {
        // something has gone wrong
        cerr << "Aborting - no fluorophores or fluorophore input file " << \
        fluorophore_inputfile << " corrupt." << endl;
        return -1;
    }
    
    if (estimate_only) {
        PrintEstimate();
        return 0;
    }   
    
    fns << output_dir <<  output_mask << make_timestamp();
    
    if (output_dir.size()>0) {
        #if defined(_WIN32)
        _mkdir(output_dir.c_str());
        #else 
        mkdir(output_dir.c_str(), 0777);                // notice that 777 is different than 0777
        #endif
    }
    
    string pname = fns.str(); pname += ".param";
    PrintParameterFile(pname);
    
    fluorophore = new simSTEDfluorophore[Nfluorophore];
    fluoImage = gsl_matrix_calloc(nPixel, nPixel);
    
    // generate output mask
    for (int kk=0; kk<Nfluorophore; kk++) {
        fluorophore[kk].setParameters(parameter);
        fluorophore[kk].setS0(baseImage[kk].intensity);
        if (inImage(baseImage[kk])) {
            gsl_matrix_set(fluoImage, baseImage[kk].xx, baseImage[kk].yy, baseImage[kk].intensity);
        } 
    }
    
    pname = fns.str(); pname += "_input.img";
    PrintImageFile(fluoImage, pname);
    pname = fns.str(); pname += "_in.fluo";
    PrintFluorophores(pname);    
    
    gsl_matrix_set_zero(fluoImage);
    
    double dx2[Nfluorophore];
    
    #ifdef DEBUG
    if (DEBUG > 0) {
        gsl_vector *vec = gsl_vector_alloc(5);
        gsl_vector_set_zero(vec); gsl_vector_set(vec, 0, 1.0);
        double r = 0.0;
        
        PrintPower(r);
        fluorophore[0].setState(vec);
        fluorophore[0].cycle(airy(r),donut(r),80);
        
        r = 25e-9; cout << endl << endl;
        PrintPower(r);
        fluorophore[0].setState(vec);
        fluorophore[0].cycle(airy(r/parameter.getAiryExc()),\
        donut(r/parameter.getAirySTED()),80);
        r = 50e-9; cout << endl << endl;
        PrintPower(r);
        fluorophore[0].setState(vec);
        fluorophore[0].cycle(airy(r/parameter.getAiryExc()),\
        donut(r/parameter.getAirySTED()),80);
        
        r = 75e-9; cout << endl << endl;
        PrintPower(r);
        fluorophore[0].setState(vec);
        fluorophore[0].cycle(airy(r/parameter.getAiryExc()),\
        donut(r/parameter.getAirySTED()),80);
        gsl_vector_free(vec);
        return -1;
    }
    #endif
    
    // run simulation
    if (nThreads < 1) nThreads = max(0, min(15, Nfluorophore/200))+1;
    if (!quiet) {
        cout << "msg: will run with " << nThreads << " threads for parallelization." << endl;
        display_progress(0,20,fns.str());
    }
    
    double signal;
    int RESCue_switch = 1;
    
    #pragma omp parallel num_threads(nThreads) default(shared) if(Nfluorophore>200) 
    for (int rr=0; rr<Nrepeat; rr++) {
        double baseProgress = (double)rr/Nrepeat;
        int curr_line = 0;
        for (int ii=0; ii<nPixel; ii++) {
            #pragma omp single 
            if (!quiet) display_progress(baseProgress+(double)ii/nPixel/Nrepeat, 20, fns.str());
            curr_line = (lineSkip*ii)%nPixel;
            
            #pragma omp for
            for (int kk=0; kk<Nfluorophore; kk++) {
                double dx = baseImage[kk].xx-curr_line;
                dx2[kk]=dx*dx;
            }
            // there is an automatic barrier here after the for construct, so all threads are synchronized now.
            
            for (int jj=0; jj<nPixel; jj++) {
                #pragma omp single
                signal = 0.0;
                #pragma omp for \
                    schedule(guided) \
                    reduction(+:signal)                 
                for (int kk=0; kk<Nfluorophore; kk++) {
                    double dy = baseImage[kk].yy-(double) jj;
                    double r=sqrt(dx2[kk] + dy*dy);
                    fluorophore[kk].cycle(airy(r/pxlPerAiry),donut(r/pxlPerDonut),NcyclesperPixel);
                    signal += fluorophore[kk].getSignal()*pinholeAttenuation(r);
                    fluorophore[kk].setSignal(0.0);
                }
                // there is an automatic barrier here after the for construct, so all threads are syncronized now.
            
                // add signal to matrix only once (variables are shared amongs threads)
                #pragma omp single
                addToMatrix(fluoImage,curr_line,jj,signal);

                // RESCue implementation

                if (partialN > 1) {
                    #pragma omp single
                    if ((signal < partiallowerThreshold) | (signal > partialupperThreshold)) {
                            // set laser power OFF
                            RESCue_switch = 0;
                    } else {
                            // keep laser power ON
                            RESCue_switch = 1;
                    }
                    #pragma omp single
                    signal = 0.0;
                    // there is an automatic barrier here after the for construct, so all threads are syncronized now.
                    
                    // (partialN - 1) iterations of dwelling on the same pixel
                    #pragma omp for \
                            schedule(guided) \
                            reduction(+:signal) 
                    for (int kk=0; kk<Nfluorophore; kk++) {
                        double dy = baseImage[kk].yy-(double) jj;
                        double r=sqrt(dx2[kk] + dy*dy);
                        fluorophore[kk].cycle(RESCue_switch*airy(r/pxlPerAiry),RESCue_switch*donut(r/pxlPerDonut),(partialN-1)*NcyclesperPixel);
                        signal += fluorophore[kk].getSignal()*pinholeAttenuation(r);   
                        fluorophore[kk].setSignal(0.0);
                    }

                    #pragma omp single
                    addToMatrix(fluoImage,curr_line,jj,signal);
                }
                // /RESCue implementation
            }
        }
    }

    if (!quiet) {
        display_progress(1,20,fns.str());
        cout << endl;
    }
    
    pname = fns.str(); pname += "_final.fluo";
    PrintFluorophores(pname);
    
    pname = fns.str(); pname += "_result.img";
    PrintImageFile(fluoImage, pname);
    
    pname = fns.str(); pname += "_bleached.img";
    gsl_matrix_set_zero(fluoImage);
    for (int kk=0; kk<Nfluorophore; kk++) {
        gsl_matrix_set(fluoImage, \
        baseImage[kk].xx, baseImage[kk].yy,
        fluorophore[kk].getB());
    }
    PrintImageFile(fluoImage, pname);
    
    gsl_matrix_free(fluoImage); 
}

// EOF
